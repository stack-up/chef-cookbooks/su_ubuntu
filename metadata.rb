name 'su_ubuntu'
maintainer 'Stack Up (https://stack-up.net)'
maintainer_email 'maintainer@stack-up.net'
license 'BSD 2-Clause License'
description 'Installs/Configures su_ubuntu'
long_description 'Installs/Configures su_ubuntu'
version '0.1.0'
chef_version '>= 13.2' if respond_to?(:chef_version)

issues_url 'https://gitlab.com/stack-up/chef-cookbooks/su_ubuntu/issues'
source_url 'https://gitlab.com/stack-up/chef-cookbooks/su_ubuntu'
